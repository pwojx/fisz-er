#!/usr/bin/env python3

import tkinter as tk
from random import choice, shuffle
from re import fullmatch
from functools import partial

root = tk.Tk()
root.title('Fisz-er v0.1')
root.geometry('640x480')
root.resizable(width=False, height=False)

question_id = None
answers_content = None
correct_answer = None
answered_questions = dict()


with open('pytania.txt') as f:
    lines = [line.strip() for line in f.readlines() if not line.strip().startswith('#')]
    
    question = list()
    questions = list()
    for line in lines:
        if len(line.strip()) > 0:
            question.append(line)
        elif len(question) > 1:
            questions.append(question)
            question = list()
        else:
            question = list()
    
    if len(lines[-1].strip()) > 1 and len(question) > 0:
            questions.append(question)
            question = list()


def next_question():
    global question_id
    global answers_content
    global correct_answer
    global mode
    global answered_questions

    while True:
        random_question = choice(questions) 
        if random_question[0].split('.')[0].isdigit():
            question_id = random_question[0].split('.')[0]
            if mode.get() == 0 or \
                    (mode.get() == 1 and question_id not in answered_questions.keys()):
                break
            elif question_id in answered_questions.keys() and \
                    len(answered_questions) == len(questions):
                random_question = ['ODPOWIEDZIAŁEŚ NA WSZYSTKIE PYTANIA', '']
                button_reset.pack(side='top')
                break
        else:
            question_id = None
            break
    question_content.set(random_question[0])

    for i in range(4):
        button_answer[i]['state'] = 'normal'
        button_answer[i]['bg'] = '#d9d9d9'

    # Questions with 4 possible answers.
    if fullmatch(r'^[1Aa]\..+$', random_question[1]) and len(random_question) == 5:
        correct_answer = random_question[1]
        answers_content = [correct_answer] + [answer for answer in random_question[2:]]
        shuffle(answers_content)

        for i, content in enumerate(answers_content):
            answers[i].set(content.split('.')[1].strip())

    # Questions with just 1 answer (or questions whose form is incorrect).
    elif len(random_question) >= 2:
        correct_answer = None
        answers[0].set(random_question[1])
        for i in range(1, 4):
            answers[i].set('')
            button_answer[i]['state'] = 'disabled'

    # Something else.
    else:
        for i in range(4):
            button_answer[i]['state'] = 'disabled'


def check_answer(i):
    global question_id
    global correct_answer
    global answered_questions

    c_ans_id = answers_content.index(correct_answer) if correct_answer is not None else 0
    if i == c_ans_id:
        button_answer[i]['bg'] = '#00ff00'
        if question_id is not None:
            answered_questions[question_id] = 1
    else:
        button_answer[i]['bg'] = '#ff0000'
        button_answer[c_ans_id]['bg'] = '#00ff00'

    for i in range(4):
        button_answer[i]['state'] = 'disabled'


def reset_answers():
    global mode
    global answered_questions
    answered_questions.clear()
    button_reset.pack_forget()


button_reset = tk.Button(root, text='Reset', command=reset_answers)

question_content = tk.StringVar()
tk.Label(root, textvariable=question_content, height=12, wraplength=600, relief='raised').pack(fill='x')

answers = list()
button_answer = list()
for i in range(4):
    answers.append(tk.StringVar())
    answers[i].set(f'Odp {i+1}')
    button_answer.append(tk.Button(root, textvariable=answers[i], wraplength=600, \
            state='disabled', disabledforeground='#191919', command=partial(check_answer, i)))
    button_answer[i].pack(fill='x')

tk.Button(root, text='Następne pytanie', command=next_question).pack(side='bottom', pady=10)

mode = tk.IntVar()
checkbox_mode = tk.Checkbutton(root, text='Nie zadawaj pytań, na które odpowiedziano poprawnie', \
        variable=mode, onvalue=1, offvalue=0)
checkbox_mode.pack(side='bottom')

next_question()

try:
    root.mainloop()
except KeyboardInterrupt:
    root.destroy()
    print('Exit')
