##########################################################################################
# Linie rozpoczynające się od '#' są ignorowane.
#
# Poprawna forma pytania to:
# [<id>. ]Treść pytania\n
# (1|A|a). POPRAWNA odpowiedź\n
# (2|B|b). Odpowiedź 2\n
# (3|C|c). Odpowiedź 3\n
# (4|D|d). Odpowiedź 4\n
# \n
#
# Fragmenty w nawiasach kwadratowych [] są opcjonalne.
# W nawiasach okrągłych () podane są znaki możliwe do użycia w danym miejscu.
# Podanie <id> jest opcjonalne, jednak zapewnia ono dodatkową funkcjonalność:
# -- możliwe jest niezadawanie ponownie pytań, na które udzielono poprawnej odpowiedzi.
# <id> powinny (nie muszą) być unikatowe.
#
# Opcjonalnie możliwe jest dodawanie pytań w formie:
# [<id>. ]Treść pytania\n
# Jedyna odpowiedź\n
# \n
#
# Odpowiedzi udzielone na pytania sformułowane w powyższy sposób nie są weryfikowane.
##########################################################################################

1. Pytanie nr 1
1. Odpowiedź nr 1
2. Odpowiedź nr 2
3. Odpowiedź nr 3
4. Odpowiedź nr 4

2. Pytanie nr 2
A. Odpowiedź A
B. Odpowiedź B
C. Odpowiedź C
D. Odpowiedź D

3. Pytanie nr 3
a. Odpowiedź a
b. Odpowiedź b
c. Odpowiedź c
d. Odpowiedź d

4. Pytanie nr 4
Jedyna odpowiedź
